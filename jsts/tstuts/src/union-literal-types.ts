type NumStr = number | string

const combine = (
  input1: NumStr,
  input2: NumStr,
  resultType: 'as-text' | 'as-number'
  ) => {
  let result;
  if (typeof input1 === 'number' &&
      typeof input2 === 'number' &&
      resultType === 'as-number') {
        result = +input1 + +input2
      } else {
        result = input1.toString() + ' ' + input2.toString()
      }
  return result    
}

const combinedNumbers = combine(30, 21, 'as-text')
console.log(combinedNumbers)

const combinedAges = combine(10, 23, 'as-number')
console.log(combinedAges)

const combinedNames = combine('Spider', 'Man', 'as-text')
console.log(combinedNames)