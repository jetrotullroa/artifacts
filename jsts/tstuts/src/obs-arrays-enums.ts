enum Role { ADMIN, EDITOR, WRITER};

type Person = {
  name: string;
  age: number;
  hobbies: string[]
  role: number;
}

const person: Person = {
  name: 'Jetz',
  age: 21,
  hobbies: ['Sports', 'Cooking'],
  role: Role.EDITOR,
}

let favoriteActivities: string[];

const toUpperCaseName = (name: string) => {
  return name.toLocaleUpperCase()
}
const toLowerCaseHobbies = (hobbies: string[]) => {
  return hobbies.map(hobby => hobby.toLowerCase())
}

const getRoleReadable = (role: number) => {
  if (role === 0) {
    return 'Admin'
  }

  if (role === 1) {
    return 'Editor'
  }

  return 'Writer'
}

const getDetails = (user: Person) => {
  return `Hi I am ${user.name}, ${user.age} years old, the ${getRoleReadable(user.role)}`
}

console.log(toUpperCaseName(person.name))
console.log(toLowerCaseHobbies(person.hobbies))
console.log(getDetails(person))