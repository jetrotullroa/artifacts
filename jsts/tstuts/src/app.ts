class Department {
  private employees: string[] = []

  constructor(id: number, name: string) {}

  addEmployee(employee: string) {
    this.employees.push(employee)
  }
}

const accounting = new Department(1, 'Accounting')

console.log(accounting)
console.log(accounting.addEmployee('Diana'))

class ITDepartment extends Department {
  constructor(id: number, admins: string[]) {
    super(id, 'IT');
  }
}

const itDepartment = new ITDepartment(2, ['Senor'])
console.log(itDepartment)
console.log(itDepartment.addEmployee('Grimm'))