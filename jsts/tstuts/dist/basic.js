"use strict";
function addTwoNumber(num1, numb2, showResult, phrase) {
    if (showResult) {
        return `${phrase} ${num1 + numb2}`;
    }
    return num1 + numb2;
}
const n1 = 7;
const n2 = 8.5;
const showResult = true;
const phrase = 'Result is:';
console.log(addTwoNumber(n1, n2, showResult, phrase));
//# sourceMappingURL=basic.js.map