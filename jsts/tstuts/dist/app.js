"use strict";
class Department {
    constructor(id, name) {
        this.employees = [];
    }
    addEmployee(employee) {
        this.employees.push(employee);
    }
}
const accounting = new Department(1, 'Accounting');
console.log(accounting);
console.log(accounting.addEmployee('Diana'));
class ITDepartment extends Department {
    constructor(id, admins) {
        super(id, 'IT');
    }
}
const itDepartment = new ITDepartment(2, ['Senor']);
console.log(itDepartment);
console.log(itDepartment.addEmployee('Grimm'));
//# sourceMappingURL=app.js.map