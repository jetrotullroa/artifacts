"use strict";
var Role;
(function (Role) {
    Role[Role["ADMIN"] = 0] = "ADMIN";
    Role[Role["EDITOR"] = 1] = "EDITOR";
    Role[Role["WRITER"] = 2] = "WRITER";
})(Role || (Role = {}));
;
const person = {
    name: 'Jetz',
    age: 21,
    hobbies: ['Sports', 'Cooking'],
    role: Role.EDITOR,
};
let favoriteActivities;
const toUpperCaseName = (name) => {
    return name.toLocaleUpperCase();
};
const toLowerCaseHobbies = (hobbies) => {
    return hobbies.map(hobby => hobby.toLowerCase());
};
const getRoleReadable = (role) => {
    if (role === 0) {
        return 'Admin';
    }
    if (role === 1) {
        return 'Editor';
    }
    return 'Writer';
};
const getDetails = (user) => {
    return `Hi I am ${user.name}, ${user.age} years old, the ${getRoleReadable(user.role)}`;
};
console.log(toUpperCaseName(person.name));
console.log(toLowerCaseHobbies(person.hobbies));
console.log(getDetails(person));
//# sourceMappingURL=obs-arrays-enums.js.map