import React, { useState } from 'react'
import { View, Text } from 'react-native'
import ColorCounter from '../components/ColorCounter'

const SquareScreen = () => {
  const [red, setRed] = useState(0)
  const [blue, setBlue] = useState(0)
  const [green, setGreen] = useState(0)
  const COLOR_INCREMENT = 15
  const COLOR_MAX_VALUE = 255
  const COLOR_MIN_VALUE = 0

  const handleIncrease = state =>
    state + COLOR_INCREMENT <= COLOR_MAX_VALUE
      ? state + COLOR_INCREMENT
      : state;
  const handleDecrease = state =>
    state - COLOR_INCREMENT >= COLOR_MIN_VALUE
      ? state - COLOR_INCREMENT
      : state;

  return (
    <View>
      <Text>Square Screen Demo</Text>
      <View
        style={{
          height: 200,
          width: 200,
          backgroundColor: `rgb(${red}, ${blue}, ${green})`
        }}
      />
      <ColorCounter
        value={red}
        onIncrease={() => setRed(handleIncrease(red))}
        onDecrease={() => setRed(handleDecrease(red))}
        color="Red"
      />
      <ColorCounter
        value={blue}
        onIncrease={() => setBlue(handleIncrease(blue))}
        onDecrease={() => setBlue(handleDecrease(blue))}
        color="Blue"
      />
      <ColorCounter
        value={green}
        onIncrease={() => setGreen(handleIncrease(green))}
        onDecrease={() => setGreen(handleDecrease(green))}
        color="Green"
      />
    </View>
  );
}

export default SquareScreen;