import React from 'react';
import { Text, StyleSheet, View } from 'react-native';

const renderTitle = () => <Text style={style.textTitle}>Getting started with react native</Text>;
const renderSubheader = name =>  <Text style={style.textSubheader}>Student name: {name}</Text>
const ComponentScreen = () => {
  const fullname = 'Jets X'
  return (
    <View style={styles.container}>
      {renderTitle()}
      {renderSubheader(fullname)}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 10
  },
  textTitle: {
    fontSize: 30,
    color: 'green',
    textAlign: 'center'
  },
  textSubheader: {
    fontSize: 15,
    color: 'red',
    textAlign: 'right'
  }
});

export default ComponentScreen;