import React from 'react';
import { View, Text, FlatList } from 'react-native';
import ImageDetail from '../components/ImageDetail'

const ImageScreen = () => {
  const cards = [
    {
      id: '65671a6tsAB12A',
      score: 11,
      image:
        'https://img.scryfall.com/cards/large/front/f/8/f8f03bb2-313e-4688-945f-052eed678174.jpg?1572491394',
      name: 'Forest',
      detail:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sed condimentum eros. Sed eu libero porttitor, feugiat arcu et, consectetur tellus.'
    },
    {
      id: '956ZDa6tsAB34A',
      score: 13,
      image:
        'https://img.scryfall.com/cards/large/front/1/8/184a196e-8604-49d2-a66a-6f7c0eafd5de.jpg?1563900065',
      name: 'Swamp',
      detail:
        'Curabitur in quam eget velit congue scelerisque pulvinar sit amet ante. Phasellus ut enim libero. Nullam mollis tincidunt erat viverra hendrerit.'
    },
    {
      id: 'A5121a6tsAB12V',
      score: 3,
      image:
        'https://img.scryfall.com/cards/large/front/3/2/32982ed2-96e4-4cc5-8562-744b06bca239.jpg?1572491355',
      name: 'Mountain',
      detail:
        'Aliquam dignissim, ex ut interdum pretium, massa metus pellentesque ex, vitae laoreet nulla mauris at felis. Integer nec nulla quis risus posuere sollicitudin.'
    }
  ];
  
  return (
    <View>
      <Text>This is an Image Screen</Text>
      <FlatList
        keyExtractor={(item) => item.id }
        data={cards}
        renderItem={({item}) => {
          return <ImageDetail data={item} /> 
        }}
      />
    </View>
  );
}

export default ImageScreen;