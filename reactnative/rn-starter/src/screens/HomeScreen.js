import React from 'react';
import { Text, StyleSheet, View, TouchableOpacity } from 'react-native';

const handlePress = ({ navigation }, location) => navigation.navigate(location);

const HomeScreen = (props) => {
  return (
    <View>
      <Text style={styles.titleStyle}>Welcome to Fwends</Text>
      <TouchableOpacity
        title="Go to Components Demo"
        onPress={() => handlePress(props, 'Components')}
      >
        <Text style={styles.buttonStyle}>Go to Components Demo</Text>
      </TouchableOpacity>
      <TouchableOpacity onPress={() => handlePress(props, 'List')}>
        <Text style={styles.buttonStyle}>Go to List Demo</Text>
      </TouchableOpacity>
      <TouchableOpacity onPress={() => handlePress(props, 'Image')}>
        <Text style={styles.buttonStyle}>Go to Image Demo</Text>
      </TouchableOpacity>
      <TouchableOpacity onPress={() => handlePress(props, 'CounterScreen')}>
        <Text style={styles.buttonStyle}>Go to Counter Demo</Text>
      </TouchableOpacity>
      <TouchableOpacity onPress={() => handlePress(props, 'ColorScreen')}>
        <Text style={styles.buttonStyle}>Go to Color Demo</Text>
      </TouchableOpacity>
      <TouchableOpacity onPress={() => handlePress(props, 'SquareScreen')}>
        <Text style={styles.buttonStyle}>Go to Square Screen Demo</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  titleStyle: {
    fontSize: 28,
    fontWeight: 'bold',
    fontFamily: 'serif',
    textAlign: 'center',
    margin: 10
  },
  buttonStyle: {
    textAlign: 'center',
    backgroundColor: 'green',
    color: 'white',
    padding: 5,
    borderStyle: 'solid',
    borderColor: 'black',
    borderWidth: 1,
    marginBottom: 10,
  }
});

export default HomeScreen;
