import React, { useState } from 'react'
import { View, Text, StyleSheet, Button } from 'react-native';

const CounterScreen = () => {
  const [counter, setCounter] = useState(0);
  return (
    <View style={styles.container}>
      <Text>CounterScreen App</Text>
      <Text>{counter}</Text>
      <Button
        style={styles.button}
        title="+"
        onPress={() => setCounter(counter + 1)}
      />
      <Button
        style={styles.button}
        title="-"
        onPress={() => setCounter(counter - 1)}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    marginTop: 20,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  }
});

export default CounterScreen;