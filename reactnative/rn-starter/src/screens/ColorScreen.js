import React, { useState } from 'react'
import { StyleSheet, FlatList, View, Button, Text } from 'react-native'

const ColorScreen = () => {
  const [colors, addColor] = useState([])

  return (
    <View>
      <Button
        title="Add Colors"
        onPress={() => addColor([...colors, handleGenerateRandomColor()])}
      />
      <FlatList
        keyExtractor={(item) => item}
        data={colors}
        renderItem={({ item }) =>{
          return (
            <View
              style={styles(item).box}
            />
          );
        }}
      />
    </View>
  );
}

const handleGenerateRandomColor = () => {
  const red = Math.floor(Math.random() * 256)
  const green = Math.floor(Math.random() * 256);
  const blue = Math.floor(Math.random() * 256);

  return `rgb(${red}, ${green}, ${blue})`
}

const styles = (item) =>
  StyleSheet.create({
    box: {
      height: 100,
      width: 100,
      backgroundColor: item
    }
  });

export default ColorScreen;