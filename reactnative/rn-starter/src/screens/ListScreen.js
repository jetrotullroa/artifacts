import React from 'react';
import { StyleSheet, View, Text, FlatList} from 'react-native'

const ListScreen = () => {
  const friends = [
    { id: 1, name: 'Friend one', age: 21 },
    { id: 2, name: 'Friend two', age: 19 },
    { id: 3, name: 'Friend three', age: 31 },
    { id: 4, name: 'Friend four', age: 29 },
    { id: 5, name: 'Friend five', age: 14 },
    { id: 6, name: 'Friend six', age: 31 }
  ]
  return (
    <View style={styles.container}>
      <Text style={styles.titleStyle}>Collection of Records</Text>
      <FlatList
        keyExtractor={item => `${item.id}`}
        data={friends}
        renderItem={({ item }) => {
          return (
            <View>
              <Text style={styles.boxStyle}>
                {item.name} - {item.age} years old
              </Text>
            </View>
          );
        }}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    padding: 10
  },
  titleStyle: {
    fontSize: 28,
    fontWeight: 'bold',
    fontFamily: 'serif',
    textAlign: 'center',
    marginBottom: 10
  },
  boxStyle: {
    textAlign: 'center',
    padding: 5,
    borderStyle: 'solid',
    borderColor: 'red',
    borderWidth: 1,
    marginBottom: 10
  }
})

export default ListScreen;