import React from 'react';
import { View, Text, Image, StyleSheet } from 'react-native';

const ImageDetail = ({data}) => {
  return (
    <View style={styles.container}>
      <Text>Score: {data.score ? data.score : 0}</Text>
      <View style={styles.card}>
        <View style={styles.imageContainer}>
          <Image style={styles.image} source={{ uri: data.image }} />
        </View>
        <View style={styles.detailsContainer}>
          <Text>{data.name}</Text>
          <Text>{data.detail}</Text>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 10
  },
  card: {
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
    borderStyle: 'solid',
    borderColor: 'black',
    borderWidth: 1,
    marginBottom: 10,
    position: 'relative'
  },
  imageContainer: {
    flex: 1,
    padding: 0
  },
  image: {
    width: '100%',
    height: undefined,
    aspectRatio: 1
  },
  detailsContainer: {
    padding: 10,
    flex: 1,
    padding: 10
  }
});

export default ImageDetail;