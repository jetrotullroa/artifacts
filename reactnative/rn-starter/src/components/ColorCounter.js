import React from 'react';
import { View, Text, Stylesheet, Button } from 'react-native'

const ColorCounter = ({ color, onIncrease, onDecrease, value={} }) => {
  return (
    <View>
      <Text>{color}</Text>
      <Text>{value}</Text>
      <Button onPress={() => onIncrease()} title={`Increase ${color}`} />
      <Button onPress={() => onDecrease()} title={`Decrease ${color}`} />
    </View>
  );
}

export default ColorCounter;